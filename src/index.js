import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';

import Routes from 'appconfig/routes.js';
import Store from 'appconfig/store.js';
import 'appconfig/styles.scss';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={Store}>
      <Routes />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
