import React from 'react';
import NotFoundComponent from 'components/NotFound';

export function NotFound() {
  return (
    <NotFoundComponent />
  );
} 

export default React.memo(NotFound);
