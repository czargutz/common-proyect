import { compose } from 'redux';
import { connect } from 'react-redux';

function mapStateToProps() {
  return {
    newContact: '123' 
  }
}

function mapDispatchToProps(dispatch) {
  return {
    addContact: () => console.log('OK')
  }
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
);
