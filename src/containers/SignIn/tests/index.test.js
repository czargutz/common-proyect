import renderer from 'react-test-renderer';
import { SignIn } from '../index';

it('renders correctly', () => {
  const wrapper = renderer
    .create(<SignIn>test</SignIn>)
    .toJSON();
  expect(wrapper).toMatchSnapshot();
});