import { createStore, combineReducers } from 'redux';

import SignInReducer from 'containers/SignIn/reducer';

const reducers = combineReducers({
  SignInReducer,
});

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

export default store;