import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import SignIn from 'containers/SignIn';
import NotFound from 'containers/NotFound';

export function Routes() {
	return (
		<Router>
			<Switch>
				<Route exact path="/me/signin" component={SignIn} />
				<Route path="/" component={NotFound} />
			</Switch>
		</Router>
	);
}

export default React.memo(Routes);
