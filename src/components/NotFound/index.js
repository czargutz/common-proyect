import React from 'react';
import Typography from '@material-ui/core/Typography';

import './styles.scss';

export function NotFound() {
  return(
    <div className='NotFound'>
      <Typography variant="h1" component="h1" align="center">
        404
      </Typography>
      <Typography variant="h4" component="h4" align="center">
        Page NotFound
      </Typography>
    </div>
  );
}

export default React.memo(NotFound);